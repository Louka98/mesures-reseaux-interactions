#Rapport TP Mesures

### Prendre quelques mesures de base sur le réseau scientifique:





   | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317080 | 
   | nombre de liens est   | 1049866  | 
   | degré moyen du réseau | 6.62208890914917| 
   | coefficient de clusturing |    0.6324308280637396     | 

###  le coefficient de clustering pour un réseau aléatoire de la même taille et du même degré moyen 

Le coefficient de clustering mesure la probabilité p qu'un noeud i soit relié à un autre noeud du réseau. Dans le cas d'un réseau aléatoire, la probabilité est la même pour tous les noeuds. Ce qui veut dire que Ci=p.
Donc, le coefficient de clustering moyen d'un graphe aléatoire est égal à 2.088×10−5

### Connexité du réseau:
 Si le graphe est connexe il possédera forcément une seule composante connexe
 
 on utilisant la classe ConnectedComponents et la fonction connexe.getConnectedComponentsCount()<br/>
 on trouve que notre graphe posséde une seule composante connexe donc il est connexe
 
 on peut aussi utiliser la fonction de Toolkit.isConnected() pour voir si notre réseau de colaboration est connexe
 
 ### Un réseau aléatoire de la même taille et degré moyen sera-t-il connexe
  
  un réseau aléatoire de la même taille et degré moyen sera connexe si la formule suivante est <br/>
  respecter:
  le degré moyen du réseau > ln(Nombre de noeud) or sur notre reseau 
  
  ln(Nombre de noeud)=12.666909386951092 ce qui fait qu'un réseau aléatoire de la méme 
  taille et degré moyen ne sera pas connexe
  
   Pour que le réseau aléatoire soit connexe, il faudrait que ⟨k⟩>ln(n)
  un réseau de cette taille devient connexe à partir d'un degré moyen égale à 12.7
 
  
 ### Calculer la distribution des degrés
 La distribution de degrés est la probabilité qu'un nœud choisi au hasard ait degré k.
  on uttilise la fonction Toolkit.degreeDistribution() de la classe Toolkit
  

  
  
  
 
 puis on trace les deux graphes, lineaire et log log 
 
 #### lineaire
 ![lineaire](src/main/resources/question%205%20lineaire.png)
 
 
 #### loglog
 ![loglog](src/main/resources/question%205%20log%20log.png)
 
En traçant la distribution de degrés en échelle log-log on observe une ligne droite pendant plusieurs ordres de grandeur. Cela nous indique une loi de puissance :
 
 #### puissance 
 
 ![puissance](src/main/resources/dd_dblp.png)
 
 On a y=2.7 qui est different de 0.04
 


#### distance moyenne et graphe de distibution des distances

la distance moyenne est: <d>=6.787289 

![distance](src/main/resources/distribtion_distance_scientifique.png)
 
 
On obseravant le graphe et la distance moyenne obtenu, on déduit que L'hypothèse des six degrés de séparation se confirme bien

pour que le réseau soit qualifié d'un réseau petit monde il faut que la distance moyenne du réseau soit égale à la distance max 
soit <d> =ln(N)/ln(<k>)

on a <d>=6.787289 et ln(N)/ln(<k>)= 6.700645= disatnce maximale 

 Donc on peu dire que:
  
  le réseau de colaboration scientifique est un réseau petit monde.
  
  
  ### génération d'un réseau aléatoire et un d'un réseau avec la méthode d'attachement préférentiel (Barabasi-Albert) qui ont la même taille et le même degré moyen
  
  On utilise le générateur de graph stream
  
  #### 1-Réseau aléatoire:
 
  #### Mesures de bases:
  

   
   | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317087 | 
   | nombre de liens est   | 1048020 | 
   | degré moyen du réseau | 6.610299587249756 | 
   | coefficient de clusturing |    2.2583935200977183E-5      | 
  
  ### Connexité du réseau aléatoire:
  
  le réseau n'est pas connexe
  
  ######  Distribution des degrés linéaire
  
 ![lineaire](src/main/resources/lineaire%20aleatoire.png)  

  ######  Distribution des degrés log log 
  
  ![loglog](src/main/resources/aleatoire%20log%20log.png)
  
  On remarque sur la courbe qu'il n'existe pas de ligne droite 
  
  donc abscece d'une loi de puissance 
  
  #### Distance moyenne
  
 la distance moyenne est6.907763979870038
  
  #### Distribution des distances
  
   ![distance aleatoire](src/main/resources/distribtion_distance_aleatoire.png)
  
  #### 2 réseau d'attachement preferentielle
  
  #### Mesures de bases:
  
  


 | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317082 | 
   | nombre de liens est   | 1268978 | 
   | degré moyen du réseau | 8.00409984588623 | 
   | coefficient de clusturing |    4.5097280176537       | 


  
 #### Connexité du réseau :
    
    Le réseau d'attachement preferentielle est connexe 
  
  
  
  
  
 ###### 2-1 Distribution des degrés linéaire
  ![lineaire](src/main/resources/lineaire%20attachement.png)
  
  ###### 2-2 Distribution des degrés log log
  
   ![loglog](src/main/resources/attachement%20loglog.png)
   
   On remarque des lignes droites donc présence d'une loie de puissance 
   
   ![puissance](src/main/resources/puissance%20attachement.png)
   
   
   #### Distance moyenne:
   
   la distance moyenne est 4.865127512756953
   
   #### Distribution des Distances:
   
   
   
   ![dis_dis_atta](src/main/resources/distribtion_distance_attach.png)
   
   
   ### Conclusion:
   
   On peut donc voir que les modèles théoriques de génération sont assez proches de la réalité en ce qui concerne les degrés et distances,
    mais restent très loin du compte en termes de clustering
   
   
  
   
   
   
   
  
    
   
  
  
  
  
  
   