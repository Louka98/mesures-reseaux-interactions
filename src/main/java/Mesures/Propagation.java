package Mesures;

import org.graphstream.algorithm.Toolkit;
import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.RandomGenerator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceEdge;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Propagation {



    /**
     *génération du réseau scientifique à partir di fichier donné en tp
     * @return
     */

    public static Graph generationGrapheScientifique(){


        System.setProperty("org.graphstream.ui", "swing");
        Graph g = new DefaultGraph("g");
        String filePath = "C:\\Users\\loune\\Desktop\\lounes.txt";


        FileSource fs = new FileSourceEdge();

        fs.addSink(g);




        fs.addSink(g);
        try {
            fs.readAll(filePath);
        } catch( IOException ignored) {

        } finally {
            fs.removeSink(g);
        }

        return g;


    }

    /**
     * génére un réseau aléatoire
     * @return
     */

    public static Graph generationGraphAleatoire(){

        Graph rand = new SingleGraph("Random");
        Generator gen = new RandomGenerator(6.62208890914917);
        gen.addSink(rand);
        gen.begin();
        for(int i=0; i<317080; i++)
            gen.nextEvents();
        gen.end();

        return rand;
    }


    /**
     * génére un réseau avec la méthode d'attechement préférentielle
     * @return
     */


    public static Graph generationGrapheAttachement(){

        Graph atta = new SingleGraph("Barabàsi-Albert");

        Generator gena = new BarabasiAlbertGenerator(7);

        gena.addSink(atta);
        gena.begin();

        for(int i=0; i<317080; i++) {
            gena.nextEvents();
        }

        gena.end();

        return atta;


    }

    /**
     * scénario 1 On ne fait rien pour empêcher l'épidémie
     * @param g le réseau auquelle on fait une simulation
     */

    private static void scenario1(Graph g) {

        HashMap<Integer, Integer> jours_malades = new HashMap<>();
        // patient 0
        Node source = Toolkit.randomNode(g);

        source.addAttribute("isAffected");

        int nbr_infecte = 1;

        jours_malades.put(1, nbr_infecte);

        Iterator<Node> iter;

        for (int i = 2; i < 90; i++) {

            for (Node n : g) {
                if (n.hasAttribute("isAffected")) {
                    iter = n.getNeighborNodeIterator();
                    while (iter.hasNext()) {
                        Node next = iter.next();
                        double proba = Math.random();
                        double seuil_infecte = 1.0 / 7.0;
                        double seuil_gueri= 1.0 / 14.0;
                        if (proba < seuil_infecte && !next.hasAttribute("isAffected")) {
                            nbr_infecte++;
                            next.addAttribute("isAffected");

                        }

                        if (proba < seuil_gueri && next.hasAttribute("isAffected")) {

                            next.removeAttribute("isAffected");
                            nbr_infecte--;

                        }

                    }


                }


            }

            jours_malades.put(i, nbr_infecte);
        }

        afficheMap(jours_malades);

    }


    /**
     *On réussit à convaincre50 % des individus
     *de mettre à jour en permanence leur anti-virus (immunisation aléatoire)
     * @param g le réseau auquelle on fait une simulation
     */

    private static void scenario2(Graph g){


        HashMap<Integer, Integer> jours_malades = new HashMap<>();

       // convaincre50 % des individus de mettre à jour en permanence leur anti-virus
        imunise_aleatoire(g);
        // patient 0
        Node source = Toolkit.randomNode(g);

        source.addAttribute("isAffected");

        if (source.hasAttribute("imunise") ) source.removeAttribute("imunise");

        int nbr_infecte = 1;

        jours_malades.put(1, nbr_infecte);

        Iterator<Node> iter;


        for (int i = 2; i < 90; i++) {

            for (Node n : g) {
                if (n.hasAttribute("isAffected")) {
                    iter = n.getNeighborNodeIterator();
                    while (iter.hasNext()) {
                        Node next = iter.next();
                        double proba = Math.random();
                        double seuil_infecte = 1.0 / 7.0;
                        double seuil_gueri= 1.0 / 14.0;
                        if (proba < seuil_infecte && !next.hasAttribute("isAffected")  ) {
                            nbr_infecte++;
                            next.addAttribute("isAffected");

                        }

                        if(next.hasAttribute("isAffected")&& next.hasAttribute("imunise")){

                            next.removeAttribute("isAffected");

                            nbr_infecte--;
                        }

                        if(!next.hasAttribute("imunise")) {

                            if (proba < seuil_gueri && next.hasAttribute("isAffected")) {

                                next.removeAttribute("isAffected");
                                nbr_infecte--;

                            }
                        }

                    }


                }


            }

            jours_malades.put(i, nbr_infecte);
        }

        afficheMap(jours_malades);


    }

    /**
     *On réussit à convaincre 50 % des individus de convaincre un de leurs contacts
     * de mettre à jour en permanence son anti-virus (immunisation sélective).
     * @param g le réseau auquelle on fait une simulation
     */

    private static void scenario3(Graph g){


        HashMap<Integer, Integer> jours_malades = new HashMap<>();

        // Immunité séléctive

        imunise_selective(g);

        // patient 0
        Node source = Toolkit.randomNode(g);

        source.addAttribute("isAffected");

        if (source.hasAttribute("imunise") ) source.removeAttribute("imunise");

        int nbr_infecte = 1;

        jours_malades.put(1, nbr_infecte);

        Iterator<Node> iter;


        for (int i = 2; i < 90; i++) {

            for (Node n : g) {
                if (n.hasAttribute("isAffected")) {
                    iter = n.getNeighborNodeIterator();
                    while (iter.hasNext()) {
                        Node next = iter.next();
                        double proba = Math.random();
                        double seuil_infecte = 1.0 / 7.0;
                        double seuil_gueri= 1.0 / 14.0;
                        if (proba < seuil_infecte && !next.hasAttribute("isAffected")  ) {
                            nbr_infecte++;
                            next.addAttribute("isAffected");

                        }

                        if(next.hasAttribute("isAffected")&& next.hasAttribute("imunise")){

                            next.removeAttribute("isAffected");

                            nbr_infecte--;
                        }

                        if(!next.hasAttribute("imunise")) {

                            if (proba < seuil_gueri && next.hasAttribute("isAffected")) {

                                next.removeAttribute("isAffected");
                                nbr_infecte--;

                            }
                        }

                    }


                }


            }

            jours_malades.put(i, nbr_infecte);
        }

        afficheMap(jours_malades);


    }






    /**
     * Imunise alétoirement 50% de la population
     * @param g
     */
    public static void imunise_aleatoire(Graph g) {
        for (Node n : g) {
            if ((int) (Math.random() * 2 + 1) == 1)
                n.addAttribute("imunise");


        }
    }

    /**
     * Imunise selectivement
     * @param g
     */

  public static void imunise_selective(Graph g) {
      for (Node n : g) {
          if ((int) (Math.random() * 2 + 1) == 1 && n.getEdgeSet().size() != 0) {
              int voisinAleat = (int) (Math.random() * n.getEdgeSet().size() - 1);
              n.getEdge(voisinAleat).getOpposite(n).addAttribute("imunise");
          }

      }
  }

            /**
             * affiche en sortie les keys values d'une map
             * @param l mape à afficher
             */
    public static void afficheMap(HashMap<Integer, Integer> l) {
        for (Map.Entry<Integer, Integer> entry : l.entrySet()) {
            int key = entry.getKey();
            double value = entry.getValue();
            System.out.println(key + "    " + value);
            // do what you have to do here
            // In your case, another loop.
        }
    }


    public static void main (String[]args){


        //*************************** simulation des 3 scénarios sur le réseau de colaboration scientifique**********************************

        // scénario 1 réseau de colaboration scientifique
        //scenario1(generationGrapheScientifique());

        // scénario 2 réseau de colaboration scientifique
        // scenario2(generationGrapheScientifique());

        // scénario 3 réseau de colaboration scientifique
        scenario3(generationGrapheScientifique());



        //*************************** simulation des trois scénarios sur un réseau aléatoire********************************************


        // scénario 1 réseau aléatoire
       // scenario1(generationGraphAleatoire());

        // scénario 2 réseau aléatoire
       // scenario2(generationGraphAleatoire());

        // scénario 3 réseau aléatoire
       //scenario3(generationGraphAleatoire());


        //**************************************** simulation des trois scénarios sur un réseau à attachement préférentiel
        // scénario 1 méthode d'attachement préférentiel
     // scenario1(generationGrapheAttachement());

        // scénario 2 méthode d'attachement préférentiel
      //  scenario2(generationGrapheAttachement());

        // scénario 3 méthode d'attachement préférentiel
      //  scenario3(generationGrapheAttachement());
    }



}
