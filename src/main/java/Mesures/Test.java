package Mesures;

import org.bouncycastle.jce.provider.JDKKeyFactory;
import org.graphstream.algorithm.ConnectedComponents;
import org.graphstream.algorithm.Toolkit;
import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.RandomGenerator;
import org.graphstream.graph.BreadthFirstIterator;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;

import java.io.IOException;
import java.util.*;

import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceEdge;

import static org.graphstream.algorithm.Toolkit.*;


/**
 * @author lounes kaci
 */


public class Test {

    /**
     * <p>calcule de la distance moyenne</p>
     * @param graph
     * @param sample
     * @return
     */

    private static double averageDistance(Graph graph, int sample) {
        int[] dd = distanceDistribution(graph, sample);
        int sum = Arrays.stream(dd).reduce(0, Integer::sum);
        double v = 0;
        for (int i = 0; i < dd.length; i++)
            v += i * dd[i];
        return v / (double) sum;
    }

    /**
     * <p>calcule la distribution des distances </p>
     * @param graph
     * @param sample
     * @return
     */

    private static int[] distanceDistribution(Graph graph, int sample) {
        int[] dd = new int[50];
        Toolkit.randomNodeSet(graph, sample).forEach(node -> {
            BreadthFirstIterator<Node> bfi = (BreadthFirstIterator<Node>) node.getBreadthFirstIterator();
            while (bfi.hasNext())
                dd[bfi.getDepthOf(bfi.next())]++;
        });
        return dd;
    }

    /**
     * méthode qui calcule la probabilité moyenne des distances entre deux noeuds
     * @param g
     * @return
     */

    public static HashMap<Integer, Double> getDistancesDistribution(Graph g) {
        List<Node> sample = Toolkit.randomNodeSet(g, 1000);
        HashMap<Integer, Double> distrib = new HashMap<>();


        int i = 0;
        for (Node n : sample) {
            BreadthFirstIterator<Node> iter = (BreadthFirstIterator<Node>) n.getBreadthFirstIterator();

            while (iter.hasNext()) {
                int depth = iter.getDepthOf(iter.next());
                Double depthSum = distrib.get(depth);

                if (depthSum == null)
                    distrib.put(depth, 1.0);
                else
                    distrib.put(depth, depthSum + 1);
            }

            i++;

        }



        Double measuresCount = distrib.values().stream().reduce(0.0, Double::sum);
        distrib.forEach((k, v) -> distrib.put(k, v / measuresCount));

        return distrib;
    }


    /**
     *génération du réseau scientifique à partir di fichier donné en tp
     * @return
     */

    public static Graph generationGrapheScientifique(){


        System.setProperty("org.graphstream.ui", "swing");
        Graph g = new DefaultGraph("g");
        String filePath = "C:\\Users\\loune\\Desktop\\lounes.txt";


        FileSource fs = new FileSourceEdge();

        fs.addSink(g);




        fs.addSink(g);
        try {
            fs.readAll(filePath);
        } catch( IOException ignored) {

        } finally {
            fs.removeSink(g);
        }

        return g;


    }


    /**
     * calcule le nombre de noeud d'un réseau
     * @param g
     */

    public static void nombreDeNoeud(Graph g) {

        System.out.println("le nombres de noeuds est " + g.getNodeCount());

    }


    /**
     * calcule le nombre de lien d'un réseau
     * @param g
     */

    public static void nombreDeLien(Graph g){

        System.out.println("nommbres de liens est"+g.getEdgeCount());
    }


    /**
     * calcule le degré moyen d'un réseau
     * @param g
     */

    public static void degreMoyen(Graph g) {

        System.out.println("le degree moyen du graphe est " + averageDegree(g));

    }

    /**
     *calcule le coeficcient de clusturing moyen d'un réseau
     * @param g
     */

    public static void  cluster(Graph g){
        System.out.println("le coeficcient de clusturing moyen est"+averageClusteringCoefficient(g));
    }


    /**
     *teste si le réseau est connexe ou pas
     * @param g
     */

    public static void estConnexe(Graph g){

        if(isConnected(g)) System.out.println("le graphe est connexe");

        else System.out.println("le systeme n'est pas connexe");
    }

    /**
     *calcule la valeur de ln(nombre de noeuds du réseau)
     * @param g
     */
    public static void calcuLn(Graph g){
        System.out.println("ln(N)= "+ Math.log(g.getNodeCount()/Math.log(Math.exp(1))));
    }

    /**
     * calcule la distribution des degés d'un réseau
     * @param g
     */

    public static void distributionDegre(Graph g){



         int[] dd = Toolkit.degreeDistribution(g);
         for (int k = 0; k < dd.length; k++) {
         if (dd[k] != 0) {
         System.out.printf(Locale.US, "%6d%20.8f%n", k, (double)dd[k] / g.getNodeCount());
         }
          }
    }

    /**
     * génére un réseau aléatoire
     * @return
     */

    public static Graph generationGraphAleatoire(){

        Graph rand = new SingleGraph("Random");
        Generator gen = new RandomGenerator(6.62208890914917);
        gen.addSink(rand);
        gen.begin();
        for(int i=0; i<317080; i++)
            gen.nextEvents();
        gen.end();

        return rand;
    }

    /**
     * génére un réseau avec la méthode d'attechement préférentielle
     * @return
     */


    public static Graph generationGrapheAttachement(){

        Graph atta = new SingleGraph("Barabàsi-Albert");

        Generator gena = new BarabasiAlbertGenerator(7);

        gena.addSink(atta);
        gena.begin();

        for(int i=0; i<317080; i++) {
            gena.nextEvents();
        }

        gena.end();

        return atta;


    }

    public static void afficheMap(HashMap<Integer, Double> l) {
        for (Map.Entry<Integer, Double> entry : l.entrySet()) {
            int key = entry.getKey();
            double value = entry.getValue();
            System.out.println(key + "    " + value);
            // do what you have to do here
            // In your case, another loop.
        }
    }


    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[]args) throws IOException {





        //*************************** partie réseau de collaboration scientifique*******************


        System.out.println("**************************mesures sur le graphe de colaboration **********************");

        Graph science= generationGrapheScientifique();
        nombreDeNoeud(science);
        nombreDeLien(science);
        degreMoyen(science);
        estConnexe(science);
        calcuLn(science);
        cluster(science);
        distributionDegre(science);
        System.out.println("la distance moyenne est" +averageDistance(science,1000));
        afficheMap(getDistancesDistribution(science));







        //*******************************partie réseau aléatoire**************************************


        System.out.println("**************************mesures sur le graphe Aleatoire **********************");

        Graph rand= generationGraphAleatoire();
        nombreDeNoeud(rand);
        nombreDeLien(rand);
        degreMoyen(rand);
        estConnexe(rand);
        calcuLn(rand);
        cluster(rand);
        distributionDegre(rand);
        System.out.println("la distance moyenne est" +averageDistance(rand,1000));
        afficheMap(getDistancesDistribution(rand));







        //******************************partie réseau d'attachement preferentielle********************


        System.out.println("**************************mesures sur le réseau d'attachement preferentielle **********************");

        Graph atta= generationGrapheAttachement();
        nombreDeNoeud(atta);
        nombreDeLien(atta);
        degreMoyen(atta);
        estConnexe(atta);
        calcuLn(atta);
        cluster(atta);
        distributionDegre(atta);
        System.out.println("la distance moyenne est" +averageDistance(atta,1000));
        afficheMap(getDistancesDistribution(atta));


    }


    }



