#Rapport TP Mesures

### Prendre quelques mesures de base sur le réseau scientifique:





   | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317080 | 
   | nombre de liens est   | 1049866  | 
   | degré moyen du réseau | 6.62208890914917| 
   | coefficient de clusturing |    0.6324308280637396     | 

###  le coefficient de clustering pour un réseau aléatoire de la même taille et du même degré moyen 

Le coefficient de clustering mesure la probabilité p qu'un noeud i soit relié à un autre noeud du réseau. Dans le cas d'un réseau aléatoire, la probabilité est la même pour tous les noeuds. Ce qui veut dire que Ci=p.
Donc, le coefficient de clustering moyen d'un graphe aléatoire est égal à 2.088×10−5

### Connexité du réseau:
 Si le graphe est connexe il possédera forcément une seule composante connexe
 
 on utilisant la classe ConnectedComponents et la fonction connexe.getConnectedComponentsCount()<br/>
 on trouve que notre graphe posséde une seule composante connexe donc il est connexe
 
 on peut aussi utiliser la fonction de Toolkit.isConnected() pour voir si notre réseau de colaboration est connexe
 
 ### Un réseau aléatoire de la même taille et degré moyen sera-t-il connexe
  
  un réseau aléatoire de la même taille et degré moyen sera connexe si la formule suivante est <br/>
  respecter:
  le degré moyen du réseau > ln(Nombre de noeud) or sur notre reseau 
  
  ln(Nombre de noeud)=12.666909386951092 ce qui fait qu'un réseau aléatoire de la méme 
  taille et degré moyen ne sera pas connexe
  
   Pour que le réseau aléatoire soit connexe, il faudrait que ⟨k⟩>ln(n)
  un réseau de cette taille devient connexe à partir d'un degré moyen égale à 12.7
 
  
 ### Calculer la distribution des degrés
 La distribution de degrés est la probabilité qu'un nœud choisi au hasard ait degré k.
  on uttilise la fonction Toolkit.degreeDistribution() de la classe Toolkit
  

  
  
  
 
 puis on trace les deux graphes, lineaire et log log 
 
 #### lineaire
 ![lineaire](../resources/question%205%20lineaire.png)
 
 
 #### loglog
 ![loglog](../resources/question%205%20log%20log.png)
 
En traçant la distribution de degrés en échelle log-log on observe une ligne droite pendant plusieurs ordres de grandeur. Cela nous indique une loi de puissance :
 
 #### puissance 
 
 ![puissance](../resources/dd_dblp.png)
 
 On a y=2.7 qui est different de 0.04
 


#### distance moyenne et graphe de distibution des distances

la distance moyenne est: <d>=6.787289 

![distance](../resources/distribtion_distance_scientifique.png)
 
 
On obseravant le graphe et la distance moyenne obtenu, on déduit que L'hypothèse des six degrés de séparation se confirme bien

pour que le réseau soit qualifié d'un réseau petit monde il faut que la distance moyenne du réseau soit égale à la distance max 
soit <d> =ln(N)/ln(<k>)

on a <d>=6.787289 et ln(N)/ln(<k>)= 6.700645= disatnce maximale 

 Donc on peu dire que:
  
  le réseau de colaboration scientifique est un réseau petit monde.
  
  
  ### génération d'un réseau aléatoire et un d'un réseau avec la méthode d'attachement préférentiel (Barabasi-Albert) qui ont la même taille et le même degré moyen
  
  On utilise le générateur de graph stream
  
  #### 1-Réseau aléatoire:
 
  #### Mesures de bases:
  

   
   | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317087 | 
   | nombre de liens est   | 1048020 | 
   | degré moyen du réseau | 6.610299587249756 | 
   | coefficient de clusturing |    2.2583935200977183E-5      | 
  
  ### Connexité du réseau aléatoire:
  
  le réseau n'est pas connexe
  
  ######  Distribution des degrés linéaire
  
 ![lineaire](../resources/lineaire%20aleatoire.png)  

  ######  Distribution des degrés log log 
  
  ![loglog](../resources/aleatoire%20log%20log.png)
  
  On remarque sur la courbe qu'il n'existe pas de ligne droite 
  
  donc abscece d'une loi de puissance 
  
  #### Distance moyenne
  
 la distance moyenne est6.907763979870038
  
  #### Distribution des distances
  
   ![distance aleatoire](../resources/distribtion_distance_aleatoire.png)
  
  #### 2 réseau d'attachement preferentielle
  
  #### Mesures de bases:
  
  


 | Mesures | Résultats | 
   | ---      |  ------  |
   | nombre de noeuds | 317082 | 
   | nombre de liens est   | 1268978 | 
   | degré moyen du réseau | 8.00409984588623 | 
   | coefficient de clusturing |    4.5097280176537       | 


  
 #### Connexité du réseau :
    
    Le réseau d'attachement preferentielle est connexe 
  
  
  
  
  
 ###### 2-1 Distribution des degrés linéaire
  ![lineaire](../resources/lineaire%20attachement.png)
  
  ###### 2-2 Distribution des degrés log log
  
   ![loglog](../resources/attachement%20loglog.png)
   
   On remarque des lignes droites donc présence d'une loie de puissance 
   
   ![puissance](../resources/puissance%20attachement.png)
   
   
   #### Distance moyenne:
   
   la distance moyenne est 4.865127512756953
   
   #### Distribution des Distances:
   
   
   
   ![dis_dis_atta](../resources/distribtion_distance_attach.png)
   
   
   ### Conclusion:
   
   On peut donc voir que les modèles théoriques de génération sont assez proches de la réalité en ce qui concerne les degrés et distances,
    mais restent très loin du compte en termes de clustering
   
   
  
 
   
   # Prpagation dans les réseaux:
   
   ## Réponse à la premire question:
   Le taux de propagation suis la formule suivante$`λ: λ = \frac{β}{µ}`$.
   
   on a : $`β = \frac{1}{7}`$ et que $`µ = \frac{2}{28} = \frac{1}{14}`$
    ainsi le taux de propagation est `λ = 2`
   
   Le seuil épidémique du réseau est calculée par la formule `λc = < k > / < k² >`. et , k = 6.622.
   
   $`\frac{6.622}{144} = 0.046`$.
   
   Pour un réseau aléatoire de même degré moyen, on applique la formule $`\frac{1}{k + 1}`$. Cela nous donne 0.131
   
   Le seuil de propagation dans un réseau aléatoire est donc 3 fois plus élevé que dans le réseau de colaboration scientifique.
   
   
   ## Simulation des trois  scénarios sur le réseau de colaboration scientifique
   
   #### On ne fait rien pour empêcher l'épidémie:
   
   ![sceario1](../resources/scenario1_réseau_colaboration.png)
   
   On remarque que le virus se propage très rapidement est infecte un grand nombres de noeuds, et continue
   à infecte presque toute la population, et cela revient au fait de ne pas prévoir aucune stratégie
   pour bloquer la propagation du virus, et montre que la mise à jour de l'anti virus 
   deux fois par mois ne suffit pas
   
   #### On réussit à convaincre 50 % des individus de mettre à jour en permanence leur anti-virus (immunisation aléatoire)
   
   ![scenario2](../resources/scenario2_réseau_colaboration.png)
   
   On remarque que le virus se propage rapidement au début puis stagne et maintien un nombre 
   d'infections constantes vers 60 000 contaminations, et cela est du à l'effet d'immunisation 
   de 50% de la population 
   
   #### On réussit à convaincre 50 % des individus de convaincre un de leurs contacts de mettre à jour en permanence son anti-virus (immunisation sélective)
   
   ![scenario3](../resources/scenario3_réseau_colaboration.png)
   
   On remarque qe le nombres d'infectés est très réduit, et le virus est vite bloqué 
   et cela s'éxplique par l'effet de l'immunisation séléctive
   
   
   #### Question 3:
   
   Les noeuds séléctionnées au hasard dans la 2 simulation ont peu de chances d'être des hubs dans le réseau alors que leurs voisins ont plus de chances de l'être.
    Ainsi, l'immunisation sélective permet d'avoir plus de chances d'immuniser des hubs et ainsi de stopper l'infection plus tôt, et cela explique 
    que le groupe 1 va avoir un degré moyen beaucoup plus grand que le groupe 1 ou la majorié des noeuds immunisé 
    sont des noeuds isolés.
    
    #### Question 5:
    
    ## Simulez l'épidémie avec les mêmes hypothèses et les mêmes scénarios dans un réseau aléatoire
     et un réseau généré avec la méthode d'attachement préférentiel de la même taille et le même degré moyen.
     
     pour chaque scénarion on va comparé les trois réseaux:
     
     
     #### On ne fait rien pour empêcher l'épidémie:
     
     
    ![sceario1c](../resources/s_1_3.png)
    
    
      #### On réussit à convaincre 50 % des individus de mettre à jour en permanence leur anti-virus (immunisation aléatoire)
      
       ![sceario2c](../resources/s_2_3.png)
   
  #### On réussit à convaincre 50 % des individus de convaincre un de leurs contacts de mettre à jour en permanence son anti-virus (immunisation sélective)
  
    ![scenario3c](../resources/s_3_3.png)
  
  
   